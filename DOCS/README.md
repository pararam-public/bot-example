# Bots API


Бот - это специализированный тип пользователя. Под ним нельзя залогиниться, а управлять может создатель через InfoBot-а. Бот подчиняется общим правам системы, т.е. должен быть добавлен в чат в который должен писать/читать. Боту передается текст сообщений обращенных к нему, если задан обратный url. Читать все сообщения чата бот не может.

### Создание и управление ботом

Создавать и менять настройки бота можно с помощью бота InfoBot в своем Info Chat-е.

Создаем бота командой `@infobot new_bot helpbot ChatHelpBot`, где первый аргумент уникальное имя, которое должно оканчиваться на `bot`, и читаемое имя.

В ответ придет сообщение с ключом АПИ вида

`as3afgmkkmmepfbosv6o6hpph8s3fj2b9kpd0jutpts3i9ct67u`, где

`URL_KEY = as3afgmk`, первые 8 символов используются при создании url бота

`API_KEY = 6hpph8s3fj2b9kpd0jutpts3i9ct67u`, отбросим первые 20 символом и получим ключ АПИ, для отправки постов и доступа к другим АПИ (через заголовок `X-APIToken`).

Установить url на который будут приходить обращения к боту можно командой

`@infobot set_bot helpbot url http://lwr.pw/as3afgmk`, где `helpbot` - уникальное имя бота, `url` - редактируемая опция, `http://lwr.pw/as3afgmk` - значение опции, адрес, куда будут отправляться сообщения.

Другие команды можно посмотреть отправив `@infobot help`

### Отправка сообщений

`POST /bot/message`

```javascript
{
    "chat_id": 37,  // int
    "reply_no": null,  // int or null
    "text": "@all test" // text, 10k symbols limit
}
```

200 OK

```javascript
{
    "post_no": 123
}
```

Например,

```bash
/bin/echo '{"chat_id": 37, "text": "@all test"}' | curl -H "X-APIToken: 6hpph8s3fj2b9kpd0jutpts3i9ct67u" -H "Content-Type: application/json" -v -X POST -k -d @- https://api.pararam.io/bot/message
```

### Задачи

Получение списка задач

`GET /msg/task`

```javascript
{
  "tasks": [
    {
      "chat_id": 44877,
      "post_no": 73,
      "state": "open",
      "time_created": "2019-10-15T10:20:36.842545Z",
      "time_updated": "2019-10-15T10:20:36.842545Z"
    },
    ...
  ]
}
```

Изменение статуса

`POST /msg/task/{chat_id}/{post_no}`

```javascript
{
    "state": "open"  // open, done, close
}
```


Например,

```bash
/bin/echo '{"state": "done"}' | curl -H "X-APIToken: 6hpph8s3fj2b9kpd0jutpts3i9ct67u" -H "Content-Type: application/json" -v -X POST -k -d @- https://api.pararam.io/msg/task/37/123
```

